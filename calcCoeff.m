function [ US, VS, UV, VV, theta ] = calcCoeff( XSE,YSE,XSI,YSI )
%calcCoeff: calcola i coefficienti dei pannelli utili per la risoluzione
%del problema col metodo dei pannelli

    XSI = XSI(1:(end-1)); YSI = YSI(1:(end-1));
    X = [XSI;flipud(XSE)]; Y = [YSI;flipud(YSE)];
    
    n = length(X)-1; %Numero di Pannelli
    US = zeros(n,n); %Velocita Vx Sorgenti
    VS = zeros(n,n); %Velocita Vy Sorgenti
    UV = zeros(n,n); %Velocita Vx Vortici
    VV = zeros(n,n); %Velocita Vy Vortici
    theta = zeros(n,1); %Angolo del sistema di riferimento locale
    betam = zeros(n,n); Rm = zeros(n,n);
    
    for j = 1:1:n
        
        theta(j) = atan2(Y(j+1)-Y(j),X(j+1)-X(j)); %Angolo del Pannello rispetto all'asse x globale
        l = sqrt((Y(j+1)-Y(j))^2+(X(j+1)-X(j))^2); %Lunghezza Pannello i-esimo
        
        for i = 1:1:n
            
            xm = (X(i+1)+X(i))/2; ym = (Y(i+1)+Y(i))/2; %Coordinate Punto di Mezzeria
            xp = (xm-X(j))*cos(theta(j)) + (ym-Y(j))*sin(theta(j)); %Mezzeria Pannello i-esimo in coordinate locali
            yp = (ym-Y(j))*cos(theta(j)) - (xm-X(j))*sin(theta(j));
            r0 = sqrt((xp)^2+(yp)^2); %Distanza dalla mezzeria all'inizio del pannello
            rl = sqrt((xp-l)^2+(yp)^2); %Distanza dalla mezzeria alla fine del pannello
            beta = atan2(yp,xp-l) - atan2(yp,xp); %Coefficiente Beta
            R = log(rl/r0); %Coefficiente R
            
            if(i == j)
                R = 0; beta = pi;
            end
            
            US(i,j) = -R/(2*pi);
            VS(i,j) = beta/(2*pi);
            UV(i,j) = beta/(2*pi);
            VV(i,j) = R/(2*pi);
            
            Rm(i,j) = R; betam(i,j) = beta;
        end
    end
end