function Main
clear all;

%Var
code = '0012';
alpha = 0;
vinf = 1;
npan = 70;
c = 100;

% Create figure
h.f = figure('units','pixels','position',[600,600,170,175],...
             'toolbar','none','menu','none','name','Metodo dei Pannelli','numbertitle','off','resize','off');
         
           
% Create textbuttons            
%moto indisturbato
h.c(1) = uicontrol('style','text','units','pixels',...
                'position',[10,145,70,20],'string','NACA');
h.c(3) = uicontrol('style','text','units','pixels',...
                'position',[90,145,70,20],'string','n.Pannelli');   
h.c(3) = uicontrol('style','text','units','pixels',...
                'position',[10,95,70,20],'string','V infinito');
h.c(4) = uicontrol('style','text','units','pixels',...
                'position',[90,95,70,20],'string','Alpha');
            
h.c(5) = uicontrol('style','edit','units','pixels',...
                'position',[10,125,70,20],'string',code);   
h.c(6) = uicontrol('style','edit','units','pixels',...
                'position',[90,125,70,20],'string',npan);  
h.c(7) = uicontrol('style','edit','units','pixels',...
                'position',[10,75,70,20],'string',vinf);   
h.c(8) = uicontrol('style','edit','units','pixels',...
                'position',[90,75,70,20],'string',alpha);   
            
          
% Create pushbutton   
h.p = uicontrol('style','pushbutton','units','pixels',...
                'position',[10,10,70,35],'string','Cancel',...
                'callback',@esc_call);
h.p = uicontrol('style','pushbutton','units','pixels',...
                'position',[90,10,70,35],'string','OK',...
                'callback',@ok_call);
    % Pushbutton callback
    function esc_call(varargin)
        close(h.f);
    end
    function ok_call(varargin)
        vinf = (round(str2double(get(h.c(7),'String')),2));
        alpha = (round(str2double(get(h.c(8),'String')),2))*(2*pi)/360;
        npan = (round(str2double(get(h.c(6),'String')),0));
        code = get(h.c(5),'String');
        close(h.f);
        
        if rem(npan,2) ~= 0
            npan = npan +1;
            warning(['Il numero di pannelli deve essere pari. Il numero dei pannelli e stato corretto in' ' ' num2str(npan)]);
        end
        
        param = convertNacaCode(code);
        [XSE,YSE,XSI,YSI] = drawProfile(param,c,npan);
        [US,VS,UV,VV,theta] = calcCoeff(XSE,YSE,XSI,YSI);
        res = panelMethod(US,VS,UV,VV,theta,vinf,alpha);
        pressurePlot(res,vinf,alpha,theta,XSI,XSE,US,VS,UV,VV);
    end 
end