function [ param ] = convertNacaCode( code )
%convertNacaCode: dalla stringa del codice NACA restituisce i parametri (in
%centesimi di corda)

    if(all(ismember(code, '0123456789')) && length(code) >= 4 && length(code) <= 6)
        
        c = char(code);

        if(length(code) == 4)
            %Codice NACA a 4 cifre
            
            param = zeros(3,1);
            
            param(1) = str2double(c(1))/100; %Alzata Massima
            param(2) = str2double(c(2))/10; %Ascissa di Alzata Massima
            param(3) = str2double(strcat(c(3),c(4)))/100; %Spessore Massimo

        elseif(length(code) == 5)
            %Codice NACA a 5 cifre
            
            param = zeros(5,1);
            
            riflesso = str2double(c(3)); cod3c = str2double([c(1) c(2) c(3)]);
            if(riflesso)
                load('./data/naca5cifre_riflesso.mat');
                i = find(NACA5CIFRE_R == cod3c,1);
                if(isempty(i))
                    error('Codice NACA a 5 cifre inesistente');
                end
                p = NACA5CIFRE_R(i,2);
                m = NACA5CIFRE_R(i,3);
                k1 = NACA5CIFRE_R(i,4);
                k1k2 = NACA5CIFRE_R(i,5);
            else
                load('./data/naca5cifre_nonriflesso.mat');
                i = find(NACA5CIFRE_NR == cod3c,1);
                if(isempty(i))
                    error('Codice NACA a 5 cifre inesistente');
                end
                p = NACA5CIFRE_NR(i,2);
                m = NACA5CIFRE_NR(i,3);
                k1 = NACA5CIFRE_NR(i,4);
                k1k2 = 0;
            end
            param(1) = p;
            param(2) = m;
            param(3) = k1;
            param(4) = str2double(strcat(c(4),c(5)))/100; %Spessore Massimo
            param(5) = k1k2;
        else
            error('Codice NACA a 6 o piu cifre non supportato');
        end
    else
        error('Codice NACA non valido');
    end
end

