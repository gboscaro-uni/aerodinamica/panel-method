function [ res ] = panelMethod( US,VS,UV,VV,theta,vinf,alpha )
%panelMethod: calcola la matrice di influenza e i termini noti per poi
%risolvere il sistema e ritornare gli N valori delle sorgenti e la
%vorticit?

    n = length(theta); %Numero di Pannelli
    A = zeros(n+1,n+1); %Matrice di Influenza
    b = zeros(n+1,1); %Termini Noti
    
    %--- CONDIZIONE DI TANGENZA ---
    
    for i = 1:1:n
        
        b(i) = vinf*sin(theta(i)-alpha); %Termine noto i-esimo
        av = 0; %Sommatoria delle componenti vorticose
        
        for j = 1:1:n
            
            %Sorgenti
            A(i,j) = -US(i,j)*sin(theta(i)-theta(j)) + VS(i,j)*cos(theta(i)-theta(j)); %Componente j-esimo della riga i-esima delle sorgenti
            
            %Vortici
            av = av + (-UV(i,j)*sin(theta(i)-theta(j)) + VV(i,j)*cos(theta(i)-theta(j)));
        end
        
        A(i,n+1) = av; %Componente Vorticosa della riga i-esima
    end
    
    %--- CONDIZIONE DI KUTTA ---
    
    b(n+1) = -vinf*cos(theta(1)-alpha) -vinf*cos(theta(end)-alpha); %Termine noto
    av = 0;
    
    for j = 1:1:n
        
        %Sorgenti
        A(n+1,j) = US(1,j)*cos(theta(1)-theta(j)) + VS(1,j)*sin(theta(1)-theta(j)) + US(end,j)*cos(theta(end)-theta(j)) + VS(end,j)*sin(theta(end)-theta(j)); %Componente j-esimo dell'ultima riga delle sorgenti
   
        %Vortici
        av = av + UV(1,j)*cos(theta(1)-theta(j)) + VV(1,j)*sin(theta(1)-theta(j)) + UV(end,j)*cos(theta(end)-theta(j)) + VV(end,j)*sin(theta(end)-theta(j));
    end
    
    A(n+1,n+1) = av; %Componente Vorticosa della'ultima riga
    
    res = linsolve(A,b); %Risolvo il sistema per ricavare le N q e la circuitazione
end