function pressurePlot( res,vinf,alpha,theta,XSI,XSE,US,VS,UV,VV )
%liftPlot a partire dal vettore delle q e di gamme disegna il grafico del
%coefficiente di pressure per l'intradosso ed estradosso
    
    figure(1); hold on;
    
    k = length(theta);

    %--- INTRADOSSO ---
    
    XSI = (XSI(1:(end-1))); %!TOFIX usare punti mezzeria
    n = length(XSI);
    CPI = zeros(n,1);
    
    for i = 1:1:n
       
        q = 0; v = 0;
        
        for j = 1:1:k
            
            q = q + res(j)*( sin( theta(i)-theta(j) )*VS(i,j) + cos( theta(i)-theta(j) )*US(i,j) );
            v = v + res(end)*( sin( theta(i)-theta(j) )*VV(i,j) + cos( theta(i)-theta(j) )*UV(i,j) );
        end
        
        vtan = vinf*cos(theta(i)-alpha) + q + v;
        CPI(i) = 1 - (vtan)^2/(vinf)^2;
    end
    
    plot(XSI,-CPI,'-r');
    
    %--- ESTRADOSSO ---
        
    XSE = (XSE(1:(end-1)));
    XSE = flipud(XSE);
    m = length(XSE) + n;
    CPE = zeros(m-n,1);
    
    for i = (n+1):1:m
       
        q = 0; v = 0;
        
        for j = 1:1:k
            
            q = q + res(j)*( sin( theta(i)-theta(j) )*VS(i,j) + cos( theta(i)-theta(j) )*US(i,j) );
            v = v + res(end)*( sin( theta(i)-theta(j) )*VV(i,j) + cos( theta(i)-theta(j) )*UV(i,j) );
        end
        
        vtan = vinf*cos(theta(i)-alpha) + q + v;
        CPE(i-n) = 1 - (vtan)^2/(vinf)^2;
    end
    
    plot(XSE,-CPE,'-g');
    
    %Grafico
    ax = gca;
    ax.XLim = [0 1];
    ax.YLim = [min(-CPI) max(-CPE)];
    ax.XGrid = 'on';
    ax.YGrid = 'on';

    try
        ax.XAxisLocation = 'origin';
        ax.YAxisLocation = 'origin';
    catch
        hl = plot([0,c],[0,0],'-k');
        set(get(get(hl,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        %warning('Questa versione di Matlab non supporta la funzionalita AxisLocation = origin');
    end
    
    legend('cp Intradosso','cp Estradosso');
    
    hold off;
end