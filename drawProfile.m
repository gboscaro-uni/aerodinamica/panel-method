function [XSE,YSE,XSI,YSI] = drawProfile( param, c, n )
%drawProfile: dai parametri NACA disegno il profilo
    
    figure(1); hold on;
    
    %--- SUDDIVISIONE INTERVALLI ---
    
    X = zeros(n/2+1,1);
    for i = 1:1:length(X)
        X(i) = (1 + cos((i-1)*2*pi/n))/2;
    end
    
    if(length(param) == 3) %PROFILO 4 CIFRE
        
        title('Metodo dei Pannelli - NACA 4 Cifre');
    
        m = param(1); p = param(2); Smax = param(3);
        X1 = X(X(:)<p,1);
        X2 = X(X(:)>=p,1);

        f1 = @(t) (m/p^2)*(t.*2*p - t.^2); %intervallo [0,p]
        f2 = @(t) (m/(1-p)^2)*(1 - 2*p + t.*2*p - t.^2); %intervallo [p,1]
        df1 = @(t) (m/p^2)*2*(p - t); %derivata intervallo [0,p]
        df2 = @(t) (m/(1-p)^2)*2*(p - t); %derivata intervallo [p,1]
        A0 = 0.2969; A1 = -0.1260; A2 = -0.3537; A3 = 0.2843; A4 = -0.1015; %Coefficienti Spessori Corretti
        f = @(t) (Smax/(0.2)) * (A0*sqrt(t) + A1*t + A2*t.^2 + A3*t.^3 + A4*t.^4); %Equazione Spessori NACA


        if(not(p == 0)) %Profilo Non Simmetrico

            %--- DISEGNO CORDA ---

            Y1 = feval(f1,X1); Y2 = feval(f2,X2);
            DY1 = feval(df1,X1); DY2 = feval(df2,X2);
            
            %Y = [Y2;Y1];
            %h = plot(X,Y,'-k');
            %set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

            %--- DISEGNO SPESSORI ---

            %Parte Sinistra del Profilo (Prima equazione)
            DS1 = feval(f,X1); 
            theta1 = atan(DY1);
            XSE1 = X1 - DS1.*sin(theta1);
            YSE1 = Y1 + DS1.*cos(theta1);
            XSI1 = X1 + DS1.*sin(theta1);
            YSI1 = Y1 - DS1.*cos(theta1);

            %Parte Destra del Profilo (Seconda equazione)
            DS2 = feval(f,X2);
            theta2 = atan(DY2);
            XSE2 = X2 + DS2.*sin(theta2);
            YSE2 = Y2 + DS2.*cos(theta2);
            XSI2 = X2 - DS2.*sin(theta2);
            YSI2 = Y2 - DS2.*cos(theta2);

            XSE = [XSE2;XSE1]; YSE = [YSE2;YSE1];
            XSI = [XSI2;XSI1]; YSI = [YSI2;YSI1];

            h1 = plot(XSI,YSI,'-k');
            h2 = plot(XSE,YSE,'-k');

            %Nascondo la legenda
            set(get(get(h1,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            set(get(get(h2,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

        else %Profilo Simmetrico

            %--- DISEGNO CORDA ---
            
            %Y = [Y2;Y1];
            %hc = plot([0,1],[0,0],'-k');
            %set(get(get(hc,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

            %--- DISEGNO SPESSORI ---

            DS = feval(f,X2);

            XSE = X2;
            YSE = DS;
            XSI = X2;
            YSI = (-1).*DS;

            h1 = plot(XSI,YSI,'-k');
            h2 = plot(XSE,YSE,'-k');

            %Nascondo la legenda
            set(get(get(h1,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            set(get(get(h2,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        end

        hold off;
    else %PROFILO 5 CIFRE
        
        title('Metodo dei Pannelli - NACA 5 Cifre');
        
        p = param(1); m = param(2); k1 = param(3); Smax = param(4); k1k2 = param(5);
        
        X1 = X(X(:)<p,1);
        X2 = X(X(:)>=p,1);
        
        if(k1k2 == 0) %Profilo Non Riflesso
        
            f1 = @(t) ( (k1/6)*(t.^3 - 3*m.*t.^2 + m^2*(3-m).*t) );
            f2 = @(t) ( ((k1*m^3)/6)*(1-t) );
            df1 = @(t) ( (k1/6)*(3*t.^2 - 6*m.*t + m^2*(3-m)) );
            df2 = @(t) ( (-k1/6)*m^3 );
            
        else %Profilo Riflesso
            
            f1 = @(t) ( (k1/6)*((t-m).^3 - k1k2*((1-m)^3).*t - m^3.*t + m^3) );
            f2 = @(t) ( (k1/6)*(k1k2*(t-m).^3 -k1k2*((1-m)^3).*t -m^3.*t +m^3) );
            df1 = @(t) ( (k1/6)*(3*(t-m).^2 - k1k2*(1-m)^3 - m^3) );
            df2 = @(t) ( (k1/6)*(3*k1k2*(t-m).^2 -k1k2*(1-m)^3 -m^3) );
            
        end

        A0 = 0.2969; A1 = -0.1260; A2 = -0.3537; A3 = 0.2843; A4 = -0.1015; %Coefficienti Spessori Corretti
        f = @(t) (Smax/(0.2)) * (A0*sqrt(t) + A1*t + A2*t.^2 + A3*t.^3 + A4*t.^4); %Equazione Spessori NACA

        %--- DISEGNO CORDA ---

        Y1 = feval(f1,X1); Y2 = feval(f2,X2);
        DY1 = feval(df1,X1); DY2 = feval(df2,X2);
        
        %Y = [Y2;Y1];
        %h = plot(X,Y,'-k');
        %set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

        %--- DISEGNO SPESSORI ---

        %Parte Sinistra del Profilo (Prima equazione)
        DS1 = feval(f,X1); 
        theta1 = atan(DY1);
        XSE1 = X1 - DS1.*sin(theta1);
        YSE1 = Y1 + DS1.*cos(theta1);
        XSI1 = X1 + DS1.*sin(theta1);
        YSI1 = Y1 - DS1.*cos(theta1);

        %Parte Destra del Profilo (Seconda equazione)
        DS2 = feval(f,X2);
        theta2 = atan(DY2);
        XSE2 = X2 + DS2.*sin(theta2);
        YSE2 = Y2 + DS2.*cos(theta2);
        XSI2 = X2 - DS2.*sin(theta2);
        YSI2 = Y2 - DS2.*cos(theta2);

        XSE = [XSE2;XSE1]; YSE = [YSE2;YSE1];
        XSI = [XSI2;XSI1]; YSI = [YSI2;YSI1];

        h1 = plot(XSI,YSI,'-k');
        h2 = plot(XSE,YSE,'-k');

        %Nascondo la legenda
        set(get(get(h1,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        set(get(get(h2,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
end